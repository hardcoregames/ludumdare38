﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMovement : MonoBehaviour
{
    public Transform PlaneTransform;
    
    private Transform _transform;
    private Rigidbody2D _rigidbody2D;
    private Vector2 _direction;
	// Use this for initialization

    void Awake()
    {
        _rigidbody2D = GetComponent<Rigidbody2D>();
    }

	void Start ()
	{
	    _transform = gameObject.transform;
	}
	
	// Update is called once per frame
	void Update ()
	{
	    Move();
        Rotate();
        Debug.Log("Character right vector");
    }

    private void Move()
    {
        var horizontal = Input.GetAxis("Horizontal");
        var vertical = Input.GetAxis("Vertical");

        var position = new Vector3(
            _transform.position.x + (horizontal * Time.deltaTime * 2),
            _transform.position.y + (vertical * Time.deltaTime * 2),
            _transform.position.z);
        //_transform.position = 





        _direction = _transform.position - PlaneTransform.position;
        _direction.Normalize();


        _rigidbody2D.MovePosition(position);
    }

    private void Rotation()
    {
        //var angle = Quaternion.AngleAxis(Random.Range(-_scatterAngle, _scatterAngle), Vector3.right) * _direction;

    }
    
    public void Rotate()
    {
        var characterPosition = _transform.position;
        var planetPosition = PlaneTransform.position;
        characterPosition.x = characterPosition.x - planetPosition.x;
        characterPosition.y = characterPosition.y - planetPosition.y;
        var angle = Mathf.Atan2(characterPosition.y, characterPosition.x) * Mathf.Rad2Deg;
        _rigidbody2D.MoveRotation(angle - 90);
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class CanonLevel
{
    public string Name;
    public string Description;
    public int Level;
    public int Cost;
    public int Value;
    public float AttackSpeed;
}

public class GameManager : MonoBehaviour {
    public static GameManager Instance { get; private set; }

    public Transform[] Spots;

    public List<CanonLevel> LaserCanonLevels;
    public List<CanonLevel> RocketCanonLevels;
    public List<CanonLevel> IceCanonLevels;
    public List<CanonLevel> ShieldCanonLevels;
 

    public GameObject LaserCanon;
    public GameObject RocketCanon;
    public GameObject IceCanon;
    public GameObject ShieldCanon;

    public List<Canon> Canons; 

    public int Money;
    public int Wave;

    public bool GameOver;

    public int EnemiesCount;

    void Awake()
    {
        if (Instance == null)
            Instance = this;
        else
            Debug.LogError("Got a second instance of the class " + GetType());
    }

    public void AddMoney(int money)
    {
        Money += money;
        UIManager.Instance.SetMoneyText(Money);
    }

	void Start ()
    {
		StartGame();
	}

    void StartGame()
    {
        UIManager.Instance.SetWaveText(Wave);
        UIManager.Instance.SetMoneyText(Money);
        EnemiesCount = 0;
    }

    public void CreateCanon(Canon.CanonType type, int spot)
    {
        var canonGameObject = Instantiate(GetCanonByType(type), Spots[spot - 1].transform) as GameObject;
        var canon = canonGameObject.GetComponent<Canon>();
        canon.InitCanon(type, spot, GetCanonLevelsByType(type)[0]);
        //Canons.Insert(spot-1, canon);
        Canons[spot - 1] = canon;
        Money -= GetCanonLevelsByType(type)[0].Cost;
        UIManager.Instance.SetMoneyText(Money);
    }

    public void UpgradeCanon(int spot)
    {
        var canon = Canons[spot - 1];
        Money -= GetCanonLevelsByType(canon.Type)[canon.CanonLevelStats.Level].Cost;
        canon.UpgradeCanon(GetCanonLevelsByType(canon.Type)[canon.CanonLevelStats.Level]); // Level actual bigger than array index by 1
        UIManager.Instance.SetMoneyText(Money);
    }

    public bool CheckCost(int level, Canon.CanonType type)
    {
        Debug.Log("Check Cost = " + GetCanonLevelsByType(type)[level].Cost + " Monety = " + Money);
        return GetCanonLevelsByType(type)[level].Cost <= Money;
    }

    public bool CheckCanUpgrade(int spot)
    {
        var canon = Canons[spot - 1];
        if (canon.CanonLevelStats.Level == GetCanonLevelsByType(canon.Type).Count)
        {
            Debug.Log("Can't Upgrade LVL MAx");

            return false;
        }

        if (!CheckCost(canon.CanonLevelStats.Level, canon.Type))
        {
            Debug.Log("Can't Upgrade cost is too high");
            return false;
        }
        Debug.Log("Can Upgrade");

        return true;
    }

    public bool CheckCanonSpot(int spot)
    {
        if (Canons[spot - 1] != null)
        {
            return true;
        }
        return false;
    }

    public void DestroyCanon(int spot)
    {
        var canon = Canons[spot - 1];
        Canons.RemoveAt(spot - 1);
        Destroy(canon.gameObject);
    }

    public List<CanonLevel> GetCanonLevelsByType(Canon.CanonType type)
    {
        switch (type)
        {
            case Canon.CanonType.Laser:
                return LaserCanonLevels;
                break;
            case Canon.CanonType.Rocket:
                return RocketCanonLevels;
                break;
            case Canon.CanonType.Ice:
                return IceCanonLevels;
                break;
            case Canon.CanonType.Shield:
                return ShieldCanonLevels;
                break;
        }
        return null;
    }

    public GameObject GetCanonByType(Canon.CanonType type)
    {
        switch (type)
        {
            case Canon.CanonType.Laser:
                return LaserCanon;
                break;            
            case Canon.CanonType.Rocket:
                return RocketCanon;
                break;           
            case Canon.CanonType.Ice:
                return IceCanon;
                break;            
            case Canon.CanonType.Shield:
                return ShieldCanon;
                break;
        }
        return null;
    }

    public void SetGameOver(bool win)
    {
        GameOver = true;
        UIManager.Instance.SetGameOver(win);
    }
	
	void Update () 
    {
		
	}
}

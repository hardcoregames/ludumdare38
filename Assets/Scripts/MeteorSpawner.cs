﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

[Serializable]
public class Wave
{
    public int MeteorCount;
    public int DronCount;
    public int BossCount;
    public int Cooldown;
}

public class MeteorSpawner : MonoBehaviour
{
    public Wave[] Waves; 

    public Transform[] SpawnPositions;

    public GameObject EnemyMeteor;
    public GameObject EnemyDron;
    public GameObject EnemyBoss;

    public float WaveTimer;
    public float SpawnDelay = 1.0f;

    private bool _canSpawning;
    private int _waveCounter;
    private Wave _currentWave;

    void Start()
    {
        _waveCounter = 0;
        _canSpawning = true;
        SetWave(Waves[_waveCounter]);
    }

    void SetWave(Wave wave)
    {
        _currentWave = wave;
        WaveTimer = 0;
    }

    bool SpawnTime()
    {
        bool spawnTime = false;
        if (WaveTimer < _currentWave.Cooldown)
        {
            WaveTimer += Time.deltaTime;
            UIManager.Instance.SetWaveTimerText(Mathf.RoundToInt(_currentWave.Cooldown - WaveTimer));
        }
        if (WaveTimer >= _currentWave.Cooldown)
        {
            spawnTime = true;
        }
        return spawnTime;
    }

    void Update()
    {
        if (GameManager.Instance.GameOver) return;
        
        if (SpawnTime() && _canSpawning)
        {
            StartCoroutine(SpawnEnemy(EnemyMeteor, SpawnPositions[Random.Range(0, SpawnPositions.Length)], _currentWave.MeteorCount));
            StartCoroutine(SpawnEnemy(EnemyDron, SpawnPositions[Random.Range(0, SpawnPositions.Length)], _currentWave.DronCount));
            StartCoroutine(SpawnEnemy(EnemyBoss, SpawnPositions[Random.Range(0, SpawnPositions.Length)], _currentWave.BossCount));

            _waveCounter++;
            if (_waveCounter < Waves.Length)
            {
                SetWave(Waves[_waveCounter]);
                UIManager.Instance.SetWaveText(_waveCounter);
            }
            else
            {
                _canSpawning = false;
                UIManager.Instance.Timer.text = "";
            }
        }

        if (!_canSpawning && GameManager.Instance.EnemiesCount <= 0)
            GameManager.Instance.SetGameOver(true);
    }

    IEnumerator SpawnEnemy(GameObject enemy, Transform spawnPoint, int enemyCount)
    {
        for (int i = 0; i < enemyCount; i++)
        {
            CreateEnemy(enemy);
            yield return new WaitForSeconds(SpawnDelay);
        }
    }

    private void CreateEnemy(GameObject enemy)
    {
        var spawnPosition = SpawnPositions[Random.Range(0, SpawnPositions.Length)];
        Instantiate(enemy, spawnPosition.position, spawnPosition.rotation);
        GameManager.Instance.EnemiesCount++;
    }
}

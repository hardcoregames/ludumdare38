﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CanonSelectorButton : MonoBehaviour
{
    public Button Button;
    public Image Image;
    public Image Selected;

    public Sprite[] CanonSprites;

	// Use this for initialization
	void Start ()
	{
	    Image.enabled = false;
	    Selected.enabled = false;
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    public void SetCanon(Canon.CanonType type)
    {
        Image.enabled = true;
        Image.sprite = CanonSprites[(int)type];
    }

    public void SetEnable(bool enable)
    {
        Selected.enabled = enable;
    }
}

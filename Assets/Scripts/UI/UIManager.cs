﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour
{
    public static UIManager Instance { get; private set; }

    public Text PlanetHPText;
    public Text MoneyText;
    public Text WaveText;
    public Text Timer;

    #region DescriptionPanel

    public Text Name;
    public Text Value;
    public Text Cost;
    public Text AttackSpeed;
    public Text Description;

    #endregion

    public int SelectedSpot = -1;
    public Canon.CanonType CanonType;
    public int CanonCost;

    public GameObject BuildPanel;
    public GameObject UpgradePanel;

    public GameObject GameOverPanel;
    public Text GameOverText;

    public GameObject UpgradeButton;

    public List<CanonSelectorButton> CanonSelectorButtons; 

    void Awake()
    {
        if (Instance == null)
            Instance = this;
        else
            Debug.LogError("Got a second instance of the class " + GetType());
    }

	void Start () 
    {
        BuildPanel.gameObject.SetActive(true);
        UpgradePanel.gameObject.SetActive(false);
        GameOverPanel.gameObject.SetActive(false);
	}
	
	void Update () 
    {

	}

    public void SelectSpot(int spot)
    {
        if (SelectedSpot != -1)
            CanonSelectorButtons[SelectedSpot - 1].SetEnable(false);

        SelectedSpot = spot;
        bool canonExist = GameManager.Instance.CheckCanonSpot(spot);

        BuildPanel.gameObject.SetActive(!canonExist);
        UpgradePanel.gameObject.SetActive(canonExist);
        if (canonExist)
        {
            var canon = GameManager.Instance.Canons[spot - 1];
            CanonType = canon.Type;
            var level = canon.CanonLevelStats.Level;
            if (canon.CanonLevelStats.Level == GameManager.Instance.GetCanonLevelsByType(CanonType).Count)
                level -= 1;
            
            SetCanonDescription(CanonType, GameManager.Instance.GetCanonLevelsByType(CanonType)[level]); 
            CanonSelectorButtons[spot - 1].SetEnable(true);
        }
        else
        {
            CanonType = Canon.CanonType.Laser;
            SetCanonDescription(CanonType, GameManager.Instance.GetCanonLevelsByType(CanonType)[0]);
            CanonSelectorButtons[spot - 1].SetEnable(true);
        }
    }

    public void SelectType(int type)
    {
        CanonType = (Canon.CanonType)type;
        SetCanonDescription(CanonType, GameManager.Instance.GetCanonLevelsByType(CanonType)[0]);

        Debug.Log("Selected type = " +  CanonType);
    }

#region Buttons

    public void Build()
    {
        if (GameManager.Instance.CheckCost(0, CanonType))
        {
            GameManager.Instance.CreateCanon(CanonType, SelectedSpot);
            BuildPanel.gameObject.SetActive(false);
            UpgradePanel.gameObject.SetActive(true);
            CanonSelectorButtons[SelectedSpot - 1].SetEnable(true);
            CanonSelectorButtons[SelectedSpot - 1].SetCanon(CanonType);

            var canon = GameManager.Instance.Canons[SelectedSpot - 1];
            SetCanonDescription(CanonType, GameManager.Instance.GetCanonLevelsByType(CanonType)[canon.CanonLevelStats.Level]); 
        }
    }

    public void Upgrade()
    {
        if (GameManager.Instance.CheckCanUpgrade(SelectedSpot))
        {
            GameManager.Instance.UpgradeCanon(SelectedSpot);
            var canon = GameManager.Instance.Canons[SelectedSpot - 1];
            var level = canon.CanonLevelStats.Level;
            if (canon.CanonLevelStats.Level == GameManager.Instance.GetCanonLevelsByType(CanonType).Count)
                level -= 1;
            SetCanonDescription(CanonType, GameManager.Instance.GetCanonLevelsByType(CanonType)[level]);
        }
    }

    public void Destroy()
    {
        GameManager.Instance.DestroyCanon(SelectedSpot);
        CanonSelectorButtons[SelectedSpot - 1].Image.enabled = false;
        BuildPanel.gameObject.SetActive(true);
        UpgradePanel.gameObject.SetActive(false);
    }

#endregion

    public void SetCanonDescription(Canon.CanonType type, CanonLevel level)
    {
        Name.text = level.Name;
        string valueText = type == Canon.CanonType.Ice ? "Freeze Time: " : type == Canon.CanonType.Shield ? "Armor: " : "Damage: ";
        Value.text = valueText + level.Value;
        Cost.text = "Level Cost: " + level.Cost;
        AttackSpeed.text = type == Canon.CanonType.Shield ? "" : "Attack Speed: " + level.AttackSpeed;
        Description.text = level.Description;
    }

    public void SetWaveText(int wave)
    {
        WaveText.text = "Wave: " + wave;
    }   
    
    public void SetMoneyText(int money)
    {
        MoneyText.text = "Money: " + money;
    }

    public void SetPlanetHealth(int hp, int maxHealth)
    {
        if (hp <= 0)
            PlanetHPText.text = "Planet Destroyed";
        else
            PlanetHPText.text = "Planet Life: " + hp + "/" + maxHealth;
    }

    public void SetWaveTimerText(int time)
    {
        Timer.text = "Next Wave: " + time;
    }

    public void SetGameOver(bool win)
    {
        GameOverPanel.gameObject.SetActive(true);
        GameOverText.text = win ? "You Win!" : "You Lose =(";
    }

    public void RestartLevel()
    {
        Scene loadedLevel = SceneManager.GetActiveScene();
        SceneManager.LoadScene(loadedLevel.buildIndex);
    }
}

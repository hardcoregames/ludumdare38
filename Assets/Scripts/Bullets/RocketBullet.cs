﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RocketBullet : Bullet 
{
    public int Damage;

	protected override void Start ()
    {
		base.Start();
	}
	
    protected override void Update()
    {
		base.Update();
	}

    public override void LostEnemy()
    {
        _rigidbody2D.AddForce(_lastEnemyPositon.normalized * 10);
    }

    protected override void ColliedWithEnemie(Enemy enemy)
    {
        base.ColliedWithEnemie(enemy);
        enemy.RemoveHealth(Damage);
    }

    public override void InitBullet(Transform enemy, int value)
    {
        base.InitBullet(enemy, value);
        Damage = value;
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public float BulletSpeed;
    public AudioClip ExplosionSound;

    protected AudioSource _audioSource;
    protected Collider2D _collider2D;
    protected SpriteRenderer _sprite;
    protected Rigidbody2D _rigidbody2D;

    protected Transform _transform;
    protected Transform _enemyTransform;
    protected Vector3 _lastEnemyPositon;

    protected virtual void Awake()
    {
        _rigidbody2D = GetComponent<Rigidbody2D>();
        _sprite = GetComponent<SpriteRenderer>();
        _collider2D = GetComponent<Collider2D>();
        _audioSource = GetComponent<AudioSource>();
    }

    protected virtual void Start()
    {
        _transform = transform;
    }

    protected virtual void Update()
    {
	    if (_enemyTransform != null)
	    {
            _transform.position = Vector2.MoveTowards(_transform.position, _enemyTransform.position, BulletSpeed * Time.deltaTime);
	        _lastEnemyPositon = _enemyTransform.position;
            Rotate();
	    }
        else
            LostEnemy();
	}   

    public virtual void LostEnemy()
    {
        
    }

    public virtual void InitBullet(Transform enemy, int value)
    {
        _enemyTransform = enemy;
    }

    public void Rotate()
    {
        var bulletPosition = _transform.position;
        var endPositon = _enemyTransform.position;
        bulletPosition.x = bulletPosition.x - endPositon.x;
        bulletPosition.y = bulletPosition.y - endPositon.y;
        var angle = Mathf.Atan2(bulletPosition.y, bulletPosition.x) * Mathf.Rad2Deg;
        _rigidbody2D.MoveRotation(angle - 90);
    }

    protected virtual  void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.gameObject.tag == "Enemy")
        {
            var enemy = coll.transform.GetComponent<Enemy>();
            if (enemy != null && !enemy.Dead)
            {
                ColliedWithEnemie(enemy);
            }
        }
    }

    protected virtual void ColliedWithEnemie(Enemy enemy)
    {
        _sprite.enabled = false;
        _collider2D.enabled = false;
        _audioSource.PlayOneShot(ExplosionSound);
        Destroy(gameObject, ExplosionSound.length);
    }
}

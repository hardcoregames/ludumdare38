﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dron : Enemy
{
    public Transform FirePoint;
    public GameObject Bullet;

    public AudioClip ShotSound;

    private Transform _transform;
    private Transform _planetPosition;
    
    private Rigidbody2D _rigidbody2D;

    public float AttackSpeed;
    protected float NextFire;

    protected override void Awake()
    {
        base.Awake();
        _rigidbody2D = GetComponent<Rigidbody2D>();
    }

    protected override void Start()
    {
        base.Start();
        _planetPosition = GameObject.FindGameObjectWithTag("Planet").transform;
        _transform = transform;
        Rotate();
    }

    protected override void Update()
    {
        base.Update();
        if (Dead || GameManager.Instance.GameOver) return;
        if (Vector3.Distance(_transform.position, _planetPosition.position) > 4)
        {
            _transform.position = Vector2.MoveTowards(_transform.position, _planetPosition.position, _moveSpeed * Time.deltaTime);
        }
        else
        {
            if (CanAttack())
            {
                Shot();
            }
        }
        Rotate();
    }

    protected virtual void Shot()
    {
        NextFire = Time.time + AttackSpeed;

        _audioSource.PlayOneShot(ShotSound);

        var bulletGameObject = Instantiate(Bullet, FirePoint.position, FirePoint.rotation);
        var bullet = bulletGameObject.GetComponent<Bullet>();
        bullet.InitBullet(_planetPosition, Damage);
        var rigidBody = bulletGameObject.GetComponent<Rigidbody2D>();
        rigidBody.AddForce(_planetPosition.position.normalized * 10);
    }

    protected virtual bool CanAttack()
    {
        return Time.time > NextFire;
    }

    public void Rotate()
    {
        var bulletPosition = _transform.position;
        var endPositon = _planetPosition.position;
        bulletPosition.x = bulletPosition.x - endPositon.x;
        bulletPosition.y = bulletPosition.y - endPositon.y;
        var angle = Mathf.Atan2(bulletPosition.y, bulletPosition.x) * Mathf.Rad2Deg;
        _transform.eulerAngles = new Vector3(0.0f, 0.0f, angle + 90.0f);
        //_rigidbody2D.MoveRotation(angle + 90);
    }
	

}

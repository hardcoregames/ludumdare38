﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Meteor : Enemy
{
    private Transform _transform;
    private Transform _planetPosition;
    private Quaternion _rotationAngle;

    protected override void Awake()
    {
        base.Awake();
    }

	protected override void Start ()
	{
        base.Start();
	    _planetPosition = GameObject.FindGameObjectWithTag("Planet").transform;
	    _transform = transform;
        _rotationAngle = Quaternion.AngleAxis(5, Vector3.forward);

	}
	
	protected override void Update () 
    {
        base.Update();
        if (Dead) return;
        _transform.position = Vector2.MoveTowards(_transform.position, _planetPosition.position, _moveSpeed * Time.deltaTime);
        _transform.rotation = (_transform.rotation * _rotationAngle);
    }

    void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.gameObject.tag == "Planet")
        {
            coll.gameObject.SendMessage("RemoveHealth", Damage);
            PlayDeath();
        }       
        
        if (coll.gameObject.tag == "Shield")
        {
            var shield = coll.gameObject.GetComponentInParent<Shield>();
            shield.RemoveArmor(Damage);
            PlayDeath();
        }
    }
}

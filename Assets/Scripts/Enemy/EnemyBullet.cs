﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBullet : Bullet
{
    public int Damage;
    
	// Use this for initialization

    public override void InitBullet(Transform enemy, int value)
    {
        base.InitBullet(enemy, value);
        Damage = value;
    }

	// Update is called once per frame
    protected override void Update()
    {
        _transform.position = Vector2.MoveTowards(_transform.position, _enemyTransform.position, BulletSpeed * Time.deltaTime);
        _lastEnemyPositon = _enemyTransform.position;
        Rotate();
    }

    protected override void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.gameObject.tag == "Planet")
        {
            coll.gameObject.SendMessage("RemoveHealth", Damage);
            PlayDeath();
        }

        if (coll.gameObject.tag == "Shield")
        {
            var shield = coll.gameObject.GetComponentInParent<Shield>();
            shield.RemoveArmor(Damage);
            PlayDeath();
        }
    }

    protected virtual void PlayDeath()
    {
        _sprite.enabled = false;
        _collider2D.enabled = false;
        _audioSource.PlayOneShot(ExplosionSound);
        Destroy(gameObject, ExplosionSound.length);
    }
}

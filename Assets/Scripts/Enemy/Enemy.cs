﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public int Health;
    public int Damage;
    public bool Dead;
    public int Cost;
    public float MoveSpeed = 2f;
    public AudioClip DestroySound;
    private Collider2D _collider2D;
    private SpriteRenderer _sprite;
    protected AudioSource _audioSource;

    protected float _moveSpeed;
    public Color FreezeColor;
    protected Color _spriteColor;

    protected virtual void Awake()
    {
        _sprite = GetComponent<SpriteRenderer>();
        _spriteColor = _sprite.color;
        _collider2D = GetComponent<Collider2D>();
    }

	protected virtual void Start ()
	{
	    _moveSpeed = MoveSpeed;
	    _audioSource = GetComponent<AudioSource>();
	}
	
    protected virtual void Update() 
    {
		
	}

    public virtual void Freeze(float time)
    {
        _sprite.color = FreezeColor;
        _moveSpeed = 0;
        StartCoroutine("FreezeDisable", time);
    }

    private IEnumerator FreezeDisable(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        _sprite.color = _spriteColor;
        _moveSpeed = MoveSpeed;
    }

    public virtual void RemoveHealth(int damage)
    {
        Health -= damage;
        if (Health <= 0)
            PlayDeath();
         
    }

    public virtual void PlayDeath()
    {
        Dead = true;
        _sprite.enabled = false;
        _collider2D.enabled = false;
        GameManager.Instance.AddMoney(Cost);
        StartCoroutine("DestroyEnemy", 0.2f);
        GameManager.Instance.EnemiesCount--;
    }

    private IEnumerator DestroyEnemy(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        _audioSource.PlayOneShot(DestroySound);
        Destroy(gameObject, DestroySound.length);
    }


}

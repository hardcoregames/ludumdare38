﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetRotator : MonoBehaviour
{
    private Transform _planetTransform;

	void Start() 
    {
        _planetTransform = transform;	
	}
	
	void Update()
    {
        var horizontal = Input.GetAxisRaw("Horizontal");
        transform.Rotate (Vector3.forward * (-90 * horizontal) * Time.deltaTime);
	}
}

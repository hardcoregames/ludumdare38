﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetHealth : MonoBehaviour
{
    public int Health = 100;
    public int MaxHealth = 100;
    public bool Death;
	// Use this for initialization
	void Start () 
    {
        UIManager.Instance.SetPlanetHealth(Health, MaxHealth);
	}
	
	// Update is called once per frame
	void Update()
    {
		
	}

    public void RemoveHealth(int health)
    {
        Health -= health;
        UIManager.Instance.SetPlanetHealth(Health, MaxHealth);
        if (Health <= 0)
            GameManager.Instance.SetGameOver(false);
    }
}

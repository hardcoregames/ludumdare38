﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Canon : MonoBehaviour 
{
    [Serializable]
    public enum CanonType
    {
        Laser,
        Rocket,
        Ice,
        Shield
    }

    public CanonLevel CanonLevelStats;

    public int Spot;
    public CanonType Type;

    public int Damage = 1; 
    public bool DetectorEnable;
    public float AttackSpeed = 5f;
    public float Radius = 100;
    public float BulletLifeTime;

    public AudioClip ShotSound;
    public Transform DetectorStart;

    protected float NextFire;

    protected AudioSource AudioSource;
    protected SpriteRenderer CanonSprite;
    protected Transform _transform;
    
    private Quaternion _startingAngle;
    private Quaternion _stepAngle;

    public virtual void InitCanon(CanonType type, int spot, CanonLevel level)
    {
        CanonLevelStats = level;
        Type = type;
        Spot = spot;
    }

    public virtual void UpgradeCanon(CanonLevel level)
    {
        CanonLevelStats = level;
    }

    protected virtual void Awake()
    {
        CanonSprite = GetComponent<SpriteRenderer>();
        AudioSource = GetComponent<AudioSource>();
    }

	protected virtual void Start () 
    {
        _transform = transform;
	    _transform.localPosition = Vector3.zero;
	    _transform.localRotation = Quaternion.identity;
        _transform.localScale = Vector3.one;
        AudioSource.clip = ShotSound;
        _startingAngle = Quaternion.AngleAxis(-22, Vector3.forward);
        _stepAngle = Quaternion.AngleAxis(2, Vector3.forward);
	}
	
	protected  virtual void Update () 
    {
        if (GameManager.Instance.GameOver) return;

		if (DetectorEnable) DetectEnemies();
	}

    void DetectEnemies()
    {
        RaycastHit2D hit;
        var angle = _transform.rotation * _startingAngle;
        Vector2 direction = angle * Vector3.up;

        for (var i = 0; i < 22; i++)
        {

            Debug.DrawRay(DetectorStart.position, direction * 10, Color.green);
            direction = _stepAngle * direction;

            if (CanAttack())
            {
                hit = Physics2D.Raycast(DetectorStart.position, direction, Radius);
                if (hit.collider != null)
                {
                    if (hit.collider.tag == "Enemy")
                    {
                        var enemy = hit.transform.GetComponent<Enemy>();
                        if (enemy != null && !enemy.Dead)
                        {
                            Shot(hit.transform.position, enemy);
                        }

                    }
                }
            }
        }
    }

    protected virtual void Shot(Vector3 targetPosition, Enemy enemy)
    {
        AudioSource.Play();
    }

    protected virtual bool CanAttack()
    {
        return Time.time > NextFire;
    }
}

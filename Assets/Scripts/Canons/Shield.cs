﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shield : Canon
{

    public int Armor;
    public bool Destroyed;

    public SpriteRenderer ShieldSprite;
    public AudioClip ExplosionSound;

    public PolygonCollider2D ShiedlCollider;

    protected override void Start()
    {
        base.Start();
        AudioSource.clip = ExplosionSound;
    }

    public override void InitCanon(CanonType type, int spot, CanonLevel level)
    {
        base.InitCanon(type, spot, level);
        Armor = level.Value;
    }

    public void RemoveArmor(int damage)
    {
        Armor -= damage;
        if (Armor <= 0)
        {
            ShiedlCollider.enabled = false;
            ShieldSprite.enabled = false;

            AudioSource.Play();
            Destroy(gameObject, ExplosionSound.length);
        }
    }
}

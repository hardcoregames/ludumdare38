﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Laser : Canon
{
    public GameObject Bullet;
    public Transform FirePoint;

    private LineRenderer _line;
    protected override void Awake()
    {
        base.Awake();
        _line = GetComponent<LineRenderer>();
    }

    public override void InitCanon(CanonType type, int spot, CanonLevel level)
    {
        base.InitCanon(type, spot, level);
        Damage = level.Value;
    }

    public override void UpgradeCanon(CanonLevel level)
    {
        base.UpgradeCanon(level);
        Damage = level.Value;
    }

    protected override void Shot(Vector3 targetPosition, Enemy enemy)
    {
        base.Shot(targetPosition, enemy);
        NextFire = Time.time + AttackSpeed;
        enemy.RemoveHealth(Damage);

        var dir = new Vector3(targetPosition.x, targetPosition.y, 0);
        var start = new Vector3(FirePoint.position.x, FirePoint.position.y, 0);
        var end = _transform.position + dir * Radius;
        _line.enabled = true;
        _line.SetPosition(0, start);
        _line.SetPosition(1, dir);
        StopCoroutine("DisableLine");
        StartCoroutine("DisableLine", BulletLifeTime);
    }

    public void DrawRay(Vector2 position, float length = 3f)
    {
        var start = new Vector3(position.x, position.y, 0);

        Debug.DrawRay(start, position * length, Color.green);
    }

    private IEnumerator DisableLine(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        Debug.Log("Disable Line");
        _line.enabled = false;
    }
}

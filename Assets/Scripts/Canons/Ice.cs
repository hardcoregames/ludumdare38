﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ice : Canon 
{
    public GameObject Bullet;
    public Transform FirePoint;
    public int FreezeTime;

    public override void InitCanon(CanonType type, int spot, CanonLevel level)
    {
        base.InitCanon(type, spot, level);
        FreezeTime = level.Value;
    }
    public override void UpgradeCanon(CanonLevel level)
    {
        base.UpgradeCanon(level);
        FreezeTime = level.Value;
    }

    protected override void Shot(Vector3 targetPosition, Enemy enemy)
    {
        base.Shot(targetPosition, enemy);
        NextFire = Time.time + AttackSpeed;

        var bulletGameObject = Instantiate(Bullet, FirePoint.position, FirePoint.rotation);
        var bullet = bulletGameObject.GetComponent<Bullet>();
        bullet.InitBullet(enemy.transform, FreezeTime);
        var rigidBody = bulletGameObject.GetComponent<Rigidbody2D>();
        rigidBody.AddForce(targetPosition.normalized * 10);
        Destroy(bulletGameObject, BulletLifeTime);
    }
}
